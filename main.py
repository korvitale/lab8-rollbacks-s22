from wsgiref.simple_server import WSGIRequestHandler
import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT


recreate_db ="DROP SCHEMA public CASCADE; CREATE SCHEMA public;"
create_tables = """
CREATE TABLE Player (username TEXT UNIQUE, balance INT CHECK(balance >= 0));
CREATE TABLE Shop (product TEXT UNIQUE, in_stock INT CHECK(in_stock >= 0), price INT CHECK (price >= 0));
CREATE TABLE Inventory (username TEXT REFERENCES Player(username), product TEXT REFERENCES Shop(product), amount INT CHECK (amount >= 0))
""" 
fill_db = """
INSERT INTO Player (username, balance) VALUES ('Alice', 100);
INSERT INTO Player (username, balance) VALUES ('Vitaliy', 99999);

INSERT INTO Shop (product, in_stock, price) VALUES ('marshmello', 9999, 1);
"""

price_request = "SELECT price FROM Shop WHERE product = %(product)s"
total_items_request = "SELECT SUM(amount) FROM Inventory WHERE username = %(username)s"
buy_decrease_balance = f"UPDATE Player SET balance = balance - ({price_request}) * %(amount)s WHERE username = %(username)s"
buy_decrease_stock = "UPDATE Shop SET in_stock = in_stock - %(amount)s WHERE product = %(product)s"
buy_update_inventory =  "INSERT INTO Inventory (username, product, amount) VALUES (%(username)s, %(product)s, %(amount)s)"

def buy_product(conn, username, product, amount):
    with conn:
        with conn.cursor() as cur:
            obj = {"product": product, "username": username, "amount": amount}

            try:
                cur.execute(buy_decrease_balance, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong username")
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Bad balance")

            try:
                cur.execute(buy_decrease_stock, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong product or out of stock")
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Product is out of stock")
            
            try:
                cur.execute(buy_update_inventory, obj)
                cur.execute(total_items_request, obj)
                if (amount := cur.fetchone()[0]) > 100:
                    conn.rollback()
                    raise Exception(f"Player can store only 100 products in total; attempted to posses {amount} items")
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Attempt to buy a negative number of items")
            
            conn.commit()

def setup_db(conn):
    with conn:
        with conn.cursor() as cur:
          cur.execute(recreate_db)
          cur.execute(create_tables)
          cur.execute(fill_db)


if __name__ == "__main__":
    # Preparing the db
    conn = psycopg2.connect("dbname=postgres host=localhost password=admin user=postgres")
    conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT) 
    conn.autocommit = True
    setup_db(conn)
    
    # Will run
    buy_product(conn, "Vitaliy", "marshmello", 99)
    buy_product(conn, "Vitaliy", "marshmello", 1)
    # Will fail with "Player can store only 100 products in total; attempted to posses 105 items"
    buy_product(conn, "Vitaliy", "marshmello", 5)
    # Will fail with "Attempt to buy a negative number of items"
    buy_product(conn, "Vitaliy", "marshmello", -1)


    